class CommentsController < ApplicationController
	before_action :authenticate_user!
	before_action :load_commentable
	def index
	    @comments = @commentable.comments
	end

	def new
	    @comment = @commentable.comments.new
	end

	def create
		@comment = @commentable.comments.new comments_params
		# @comment.user = current_user
		@comment.save!
		redirect_to @commentable, notice: "Your comment was successfully posted."
	end

    def update
		@comment = Comment.find(params[:id])
	    if @comment.update_attributes(comment_params)
	      redirect_to @comment.commentable, notice: "Comment was updated."
	    else
	      render :edit
	    end
    end

   def destroy
    @comment.destroy
    redirect_to @comment.commentable, notice: "Comment destroyed."
   end

	private

	def load_commentable
	    resource, id = request.path.split('/')[1, 2]
	    @commentable = resource.singularize.classify.constantize.find(id)
	end

	def comments_params
		params.require(:comment).permit(:body)
	end
end